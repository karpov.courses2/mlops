FROM python:3.10.9-slim-bullseye

ENV FLASK_APP=main.py
ENV FLASK_RUN_HOST=0.0.0.0
ENV FLASK_RUN_PORT=8001
WORKDIR /app

COPY ./app .
EXPOSE 8001
RUN pip install --no-cache-dir --upgrade -r requirements.txt

CMD ["flask", "run"]
