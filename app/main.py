from flask import Flask
import time

from utils import (
    secret,
    )

app = Flask(__name__)

@app.route('/return_secret_number')
def get_secret_number():
    time.sleep(1)
    return secret.secret_number